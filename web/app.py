import logging

from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def hello():
    return "<head>200/OK</head>"

@app.errorhandler(403)
def page_forbidden(e):
   ## app.logger.waring("++ 403 error:{}".format(e))
    return flask.render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(e):
   ## app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
